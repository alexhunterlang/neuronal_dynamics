#%%

import os
import numpy as np

from keras.models import Sequential
from keras.layers import Dense, GRU
from keras.optimizers import SGD, Nadam
from keras.regularizers import l1_l2
from keras.callbacks import ModelCheckpoint, CSVLogger, EarlyStopping

# Force matplotlib to not use any Xwindows backend.
# Otherwise will get error on linux servers
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from scipy.signal import periodogram

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/neuronal_dynamics/src/'
        os.chdir(path_to_script_folder)

from load_data import loadmat


#%%
def convert_lfp(data, lahead, tsteps, match_input = False):
    
    if data.ndim == 1:
        data = data.reshape((1,data.size))
    
    num_samples = data.shape[1]-tsteps-lahead
    X = np.zeros(shape=(data.shape[0]*num_samples,tsteps))
    if match_input:
        y = np.zeros(shape=(data.shape[0]*num_samples,tsteps+lahead))
    else:
        y = np.zeros(shape=(data.shape[0]*num_samples,lahead))
    index = 0
    for i in range(data.shape[0]):
        for j in range(data.shape[1]-tsteps-lahead):
            X[index] = data[i,j:j+tsteps]
            if match_input:
                y[index] = data[i,j:(j+tsteps+lahead)]
            else:
                y[index] = data[i,(j+tsteps):(j+tsteps+lahead)]
            index += 1

    if X.ndim == 2:
        X = X.reshape(X.shape+(1,))
        
    if y.ndim == 1:
        y = y.reshape(y.shape+(1,))
    
    return X,y

#%%
def standardize_data(data , axis = None):
    
    if axis is None:
        d_max = np.max(data)
        d_min = np.min(data)
    else:
        d_max = np.max(data, axis=axis)
        d_min = np.min(data, axis=axis)
    
    data = (data-d_max)/(d_max-d_min)
    
    return data

#%%
def scale_data(data, maxmax, minmin):
    return (data-minmin)/(maxmax-minmin)

#%%
def unscale_data(data, maxmax, minmin):
    return data*(maxmax-minmin)+minmin

#%%

if __name__ == '__main__':

    save_folder = '../results/ieee_v3_match/'
    tsteps = 5
    batch_size = 32
    nb_epochs = 5
    lahead = 5
    match_input = True
    optimizer = 'nadam'
 
    
    os.mkdir(save_folder)
    
    lfp = np.zeros((10000, 64, 2001))
    i = 0
    file_contents = loadmat('../data/04182016.mat')
    exData = file_contents['exData']
    for key in exData.keys():
        lfp[i] = exData[key]['LFP']
        i += 1
        
    lfp = lfp[0:i]
    
    X_train, y_train = convert_lfp(lfp[:,0,0:500], lahead, tsteps, match_input)
    X_test, y_test = convert_lfp(lfp[:,0,500:1000], lahead, tsteps, match_input)
    
    y_true = lfp[:,0,0:1000]

    maxmax = -np.inf
    minmin = np.inf
    for d in [X_train, X_test, y_train, y_test, y_true]:
        maxmax = np.maximum(maxmax, np.max(d))
        minmin = np.minimum(minmin, np.min(d))
    

    X_train = scale_data(X_train, maxmax, minmin)
    X_test = scale_data(X_test, maxmax, minmin)
    y_train = scale_data(y_train, maxmax, minmin)
    y_test = scale_data(y_test, maxmax, minmin)
    y_true = scale_data(y_true, maxmax, minmin)

    model = Sequential()
    reg = 1e-4
    model.add(GRU(50,
                   input_shape = (tsteps, 1),
                   activation='tanh',
                   recurrent_activation='hard_sigmoid',
                   use_bias=True,
                   kernel_initializer='glorot_uniform',
                   recurrent_initializer='orthogonal',
                   kernel_regularizer=l1_l2(l1=reg,l2=reg),
                   recurrent_regularizer=l1_l2(l1=reg,l2=reg),
                   dropout=0.0,
                   recurrent_dropout=0.0))
    
    if match_input:
        model.add(Dense(tsteps+lahead))
    else:
        model.add(Dense(lahead))
    
    if optimizer == 'sgd':
        opt = SGD(lr=0.1, momentum=0.99, decay=0.0, nesterov=True, clipnorm=1.)
    elif optimizer == 'nadam':
        opt = Nadam(clipnorm=1.0)
    else:
        raise NotImplementedError
    
    model.compile(loss='mean_squared_error', optimizer=opt, metrics=['mae'])

    filepath = save_folder + 'best.hdf5'
    cb_mc  = ModelCheckpoint(filepath, monitor='val_loss',
                                             verbose=0, save_best_only=True,
                                             save_weights_only=False,
                                             mode='auto', period=1)
    
    filepath = save_folder + 'csv.txt'
    cb_csv = CSVLogger(filepath)
    
    cb_es = EarlyStopping(monitor='val_loss', min_delta=0,
                          patience=25, verbose=0, mode='auto')
    
    cb_ls = [cb_mc, cb_csv]
 
    
       
    np.savez_compressed(save_folder+'parameters',
                        tsteps = tsteps,
                        batch_size = batch_size,
                        lahead = lahead,
                        nb_epochs = nb_epochs,
                        reg = reg,
                        optimizer = opt)
    
    model.fit(X_train, y_train, batch_size=batch_size,
              epochs=nb_epochs, shuffle = True,
              validation_data=(X_test, y_test),
              callbacks = cb_ls,
              verbose = 0)

    if False:

        # TODO: this figures assumed that only cared about a single forward time step
        # Need to be able to display multiple time step predictions
        # Maybe just a matrix of predicted values
    
        # make some predictions
        index = np.arange(500-tsteps-lahead+1)
        
        model.load_weights(save_folder + 'best.hdf5', by_name=False)
    
        y_pred_train = model.predict(X_train[index], batch_size=batch_size).flatten()
        y_pred_test = model.predict(X_test[index], batch_size=batch_size).flatten()
    
        x_true = np.arange(1000)
        x_pred_train = np.arange(tsteps,500-lahead+1)
        x_pred_test = np.arange(500+tsteps,1000-lahead+1)
        
        x_ls = [x_true, x_pred_train, x_pred_test]
        y_ls = [y_true[0], y_pred_train, y_pred_test]
        color_ls = ['black','red','blue']
        label_ls = ['truth', 'train', 'test']
        fig = plt.figure()
        for x,y,c,l in zip(x_ls,y_ls, color_ls, label_ls):
            yy = unscale_data(y,maxmax,minmin)
            plt.plot(x,yy,color=c,label=l)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.xlabel('Time [ms]')
        plt.ylabel('LFP [$\mu$V]')
        plt.title('Example LFP on 04182016 channel 1')
        fig.savefig(save_folder+'example.pdf', bbox_inches='tight')
    
    
        # periodogram
        # How best to display differences
        
        fig = plt.figure()
        y = unscale_data(y_true[0,tsteps:500], maxmax, minmin)
        f, Pxx_den = periodogram(y, 1000)
        Pxx_den[0] = np.nan    
        plt.semilogy(f, Pxx_den, color='black', label = 'truth')
        y = unscale_data(y_pred_train, maxmax, minmin)
        f, Pxx_den = periodogram(y, 1000)
        Pxx_den[0] = np.nan    
        plt.semilogy(f, Pxx_den, color = 'red', label = 'train')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.xlabel('frequency [Hz]')
        plt.ylabel('PSD [${\mu V}^2}$/Hz]')
        plt.title('Power Spectrum for Training on 04182016 channel 1')
        fig.savefig(save_folder+'power_spectrum_train.pdf', bbox_inches='tight')
        
        fig = plt.figure()
        y = unscale_data(y_true[0,500+tsteps:1000], maxmax, minmin)
        f, Pxx_den = periodogram(y, 1000)
        Pxx_den[0] = np.nan    
        plt.semilogy(f, Pxx_den, color='black', label = 'truth')
        y = unscale_data(y_pred_test, maxmax, minmin)
        f, Pxx_den = periodogram(y, 1000)
        Pxx_den[0] = np.nan    
        plt.semilogy(f, Pxx_den, color = 'blue', label = 'test')
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.xlabel('frequency [Hz]')
        plt.ylabel('PSD [${\mu V}^2}$/Hz]')
        plt.title('Power Spectrum for Testing on 04182016 channel 1')
        fig.savefig(save_folder+'power_spectrum_test.pdf', bbox_inches='tight')
    
        
        
        # make some predictions going forward in time
        y_future = np.zeros((500-tsteps,))
        inputs = X_train[500]
        inputs = inputs.reshape((1,)+inputs.shape)
        for i in range(y_future.size):
            output =  model.predict(inputs).flatten()
            y_future[i] = output
            inputs = np.concatenate((inputs[:,1:],output.reshape((1,1,1))), axis=1)
    
        x_true = np.arange(500+tsteps,1000)
        x_pred_test = np.arange(500+tsteps,1000)
        
        x_ls = [x_true, x_pred_test]
        y_ls = [y_true[0][500+tsteps:1000], y_future]
        color_ls = ['black', 'blue']
        label_ls = ['truth', 'test']
        fig = plt.figure()
        for x,y,c,l in zip(x_ls,y_ls, color_ls, label_ls):
            yy = unscale_data(y,maxmax,minmin)
            plt.plot(x,yy,color=c,label=l)
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.xlabel('Time [ms]')
        plt.ylabel('LFP [$\mu$V]')
        plt.title('Predicted Future LFPs')
        fig.savefig(save_folder+'future.pdf', bbox_inches='tight')
    