#%% imports
import scipy.io as spio
import os
import numpy as np

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/neuronal_dynamics/src/'
        os.chdir(path_to_script_folder)

#%%
#['LFP',
# 'HitMiss',
# 'Side',
# 'TargPos',
# 'FixOn',
# 'HitTime',
# 'Orientation',
# 'Contrast',
# 'Block',
# 'EyeData',
# 'Pupil',
# 'Speed',
# 'TrialSpikes',
# 'SaccadeTimes',
# 'SaccadeVec']

#%%
def loadmat(filename):
    '''
    this function should be called instead of direct spio.loadmat
    as it cures the problem of not properly recovering python dictionaries
    from mat files. It calls the function check keys to cure all entries
    which are still mat-objects
    '''
    dict = spio.loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_keys(dict)

def _check_keys(dict):
    '''
    checks if entries in dictionary are mat-objects. If yes
    todict is called to change them to nested dictionaries
    '''
    for key in dict.keys():
        if isinstance(dict[key], spio.matlab.mio5_params.mat_struct):
            dict[key] = _todict(dict[key])
        elif isinstance(dict[key], np.ndarray):
            d = {}
            for i in range(dict[key].size):
                d[i] = _todict(dict[key][i])
            dict[key] = d
        
    return dict        

def _todict(matobj):
    '''
    A recursive function which constructs from matobjects nested dictionaries
    '''
    dict = {}
    for strg in matobj._fieldnames:
        elem = matobj.__dict__[strg]
        if isinstance(elem, spio.matlab.mio5_params.mat_struct):
            dict[strg] = _todict(elem)
        elif isinstance(elem,np.ndarray):
            dict[strg] = _tolist(elem)
        else:
            dict[strg] = elem
    return dict

def _tolist(ndarray):
    '''
    A recursive function which constructs lists from cellarrays 
    (which are loaded as numpy ndarrays), recursing into the elements
    if they contain matobjects.
    '''
    elem_list = []            
    for sub_elem in ndarray:
        if isinstance(sub_elem, spio.matlab.mio5_params.mat_struct):
            elem_list.append(_todict(sub_elem))
        elif isinstance(sub_elem,np.ndarray):
            elem_list.append(_tolist(sub_elem))
        else:
            elem_list.append(sub_elem)
    return elem_list

#%%
def convert_lfp(data_name, output_size, input_size):
    
    temp = np.load(data_name+'.npz')
    data = temp[data_name]
    data = data.reshape((-1,data.shape[-1]))
    
    num_samples = data.shape[1]-input_size
    X = np.zeros(shape=(data.shape[0]*num_samples,input_size))
    y = np.zeros(shape=(data.shape[0]*num_samples,))
    for i in range(data.shape[1]-input_size):
        start = i*data.shape[0]
        stop = (i+1)*data.shape[0]
        X[start:stop,:] = data[:,i:i+output_size]
        y[start:stop] = data[:,i+output_size]
    
    return X,y


#%%
if __name__ == '__main__':
    
    init_process = False
    init_lfp_early = False
    prep_ieee = True
    
    os.chdir('../data/')
    
    if init_process:
        
        file_ls = [f for f in os.listdir('.') if f.endswith('.mat')]
        
        lfp = np.zeros((10000, 64, 2001))
        i = 0
        
        filename = []
        index = []
        
        for f in file_ls:
            file_contents = loadmat(f)
            exData = file_contents['exData']
            for key in exData.keys():
                lfp[i] = exData[key]['LFP']
                i += 1
                filename.append(f)
                index.append(key)
                if i >= lfp.shape[0]:
                    lfp = np.concatenate((lfp,np.zeros((10000, 64, 2001))))
            
        
            
        kwargs = {}
        kwargs['LFP'] = lfp[0:i]
        kwargs['filename'] = filename
        kwargs['file_index'] = index
            
        np.savez_compressed('LFP', **kwargs)

    if init_lfp_early:
        lfp = np.load('LFP.npz')
        
        LFP = lfp['LFP']
        
        lfp_train_prestim = LFP[:,:,0:500]
        lfp_test_prestim = LFP[:,:,500:1000]
        
        
        kwargs = {}
        kwargs['LFP_train_prestim'] = lfp_train_prestim
        kwargs['filename'] = lfp['filename']
        kwargs['file_index'] = lfp['file_index']
        np.savez_compressed('LFP_train_prestim', **kwargs)
        
        kwargs = {}
        kwargs['LFP_test_prestim'] = lfp_test_prestim
        kwargs['filename'] = lfp['filename']
        kwargs['file_index'] = lfp['file_index']
        np.savez_compressed('LFP_test_prestim', **kwargs)
        
        
    if prep_ieee:
                
        input_size = 10
        output_size = 1
        
        data_name = 'LFP_train_prestim'
        X_train, y_train = convert_lfp(data_name, output_size, input_size)
        
        data_name = 'LFP_test_prestim'
        X_test, y_test = convert_lfp(data_name, output_size, input_size)
        
        kwargs = {}
        kwargs['X_train'] = X_train
        kwargs['y_train'] = y_train
        kwargs['X_test'] = X_test
        kwargs['y_test'] = y_test
        np.savez_compressed('lfp_lstm', **kwargs)
        
        subset = 10000
        kwargs = {}
        kwargs['X_train'] = X_train[0:10000]
        kwargs['y_train'] = y_train[0:10000]
        kwargs['X_test'] = X_test[0:10000]
        kwargs['y_test'] = y_test[0:10000]
        np.savez_compressed('lfp_lstm_subset', **kwargs)
        
